import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
// Ads components
import Ad from '@/components/Ads/Ad'
import AdList from '@/components/Ads/AdList'
import NewAd from '@/components/Ads/NewAd'
// Auth components
import Login from '@/components/Auth/Login'
import Registration from '@/components/Auth/Registration'
// Orders
import Orders from '@/components/User/Orders'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '',
      name: 'home',
      component: Home
    },

    {
      path: '/login',
      name: 'login',
      component: Login
    },

    {
      path: '/ad/:id',
      name: 'ad',
      component: Ad
    },

    {
      path: '/registration',
      name: 'registration',
      component: Registration
    },

    {
      path: '/orders',
      name: 'orders',
      component: Orders
    },

    {
      path: '/new',
      name: 'newAd',
      component: NewAd
    },

    {
      path: '/list',
      name: 'adList',
      component: AdList
    }
  ],
  mode: 'history'
})
